using UnityEngine;
using System.Collections;

namespace Components
{
    public class TextLanguageManager : MonoBehaviour 
    {
        [HideInInspector]
        public string []LanguageList = null;
        
        [HideInInspector]
        public int CurrentLanguageIndex = -1;

        [HideInInspector]
        public const string LanguageIsoCodeString = "IsoLanguageCode";

        /// The MultipleTextManager Instance.
        private static TextLanguageManager m_currentInstance = null;

        /// <summary>
        /// Get the class instance, or if it doesn't exist, create a new one.
        /// </summary>
        public static TextLanguageManager Instance
        {
            get
            {
                if (m_currentInstance == null)
                {
                    m_currentInstance = (TextLanguageManager)FindObjectOfType(typeof(TextLanguageManager));
                }

                // If it is still null, create a new instance
                if (m_currentInstance == null)
                {
                    GameObject obj = new GameObject("TextLanguageManager");
                    m_currentInstance = (TextLanguageManager)obj.AddComponent(typeof(TextLanguageManager));
                }

                return m_currentInstance;
            }
        }

        void Awake()
        {
            if(LanguageList == null || LanguageList.Length <= 0)
                Debug.LogWarning(this + " - Language list is empty. Retrieving texts is not possible!");
            else if(CurrentLanguageIndex <= -1)
            {
                Debug.LogWarning(this + " - No default current language is chosen. Reverting to language " + LanguageList[LanguageList.Length -1] + ".");
                CurrentLanguageIndex = (LanguageList.Length -1);
            }
        }

        public void SetLanguage(int index)
        {
            if(index < 0 || (LanguageList.Length) < index)
                Debug.LogError(this + " - Invalid index value (" + index +"). Maximum value is " + (LanguageList.Length - 1) + ".");
            else
                CurrentLanguageIndex = index;

            PlayerPrefs.SetString(LanguageIsoCodeString, LanguageList[CurrentLanguageIndex]);
        }

        public void SetLanguage(string newActiveLanguage)
        {
            int objectCount = LanguageList.Length;
            for(int i = 0; i < objectCount; ++i)
            {
                if(string.Equals(LanguageList[i], newActiveLanguage) == true)
                {
                    CurrentLanguageIndex = i;
                    break;
                }
            }

            PlayerPrefs.SetString(LanguageIsoCodeString, LanguageList[CurrentLanguageIndex]);
        }

        public string GetCurrentLanguage()
        {
            return PlayerPrefs.GetString(LanguageIsoCodeString, string.Empty);
        }

        public int GetCurrentLanguageIndex()
        {
            return CurrentLanguageIndex;
        }
        
    }
}
