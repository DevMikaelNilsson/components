using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// A manager class which stores text(s) in different languages.
/// Each language has its own set of Tag and the linked text to that specific language.
/// </summary>
namespace Components
{
	[AddComponentMenu("Components/Text/TextManager")]
	[Serializable]
	public class TextManager : MonoBehaviour
	{
		/// <summary>
		/// A scene based list with all text strings needed for this perticular scene.
		/// The list is not stored into other scenes, so some lists may need to have the same string elements.
		/// </summary>
		[SerializeField]
		private List<TextInstance> TextList = new List<TextInstance>();

		public int ObjectCount { get { return TextList.Count; } }

		/// The TextManager Instance.   
		private static TextManager m_currentInstance = null;

		/// <summary>
		/// Get the class instance, or if it doesn't exist, create a new one.
		/// </summary>
		public static TextManager Instance
		{
			get
			{
				if (m_currentInstance == null)
				{
					m_currentInstance = (TextManager)FindObjectOfType(typeof(TextManager));
				}

				// If it is still null, create a new instance
				if (m_currentInstance == null)
				{
					GameObject obj = new GameObject("TextManager");
					m_currentInstance = (TextManager)obj.AddComponent(typeof(TextManager));
				}

				return m_currentInstance;
			}
		}

		/// <summary>
		/// Internal Unity method.
		/// When the user clicks on the "Reset" option in the editor inspector, the whole script/class is resetted.
		/// So I need to re-set all the proper values when this method is called.
		/// </summary>
		void Reset()
		{
			TextList.Clear();
			ReloadTextList();
		}

		/// <summary>
		/// 
		/// </summary>
		public void ReloadTextList()
		{
			if (TextList.Count == 0)
			{

			}
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="tag">The text tag to retrieve.</param>
		/// <returns>A valid string.</returns>
		public string GetText(string tag)
		{
			if (tag == string.Empty)
			{
				Debug.LogError(this.ToString() + " - Invalid Tag value (" + tag + ").");
				return string.Empty;
			}

			int objectCount = TextList.Count;
			for (int i = 0; i < objectCount; ++i)
			{
				if (string.Equals(TextList[i].Tag, tag) == true)
				{
					return TextList[i].GetText();
				}
			}

			Debug.LogWarning(this + " - No pre-defined text(s) were found. Returning empty string.");
			return string.Empty;
		}

		/// <summary>
		/// Retrieve a Text instance from the internal text list.
		/// </summary>
		/// <param name="index">A valid index value (zero based value).</param>
		/// <returns>A valid Text instance, if found. Returns null otherwise.</returns>
		public TextInstance GetTextInstance(int index)
		{
			if (index < 0 || index > TextList.Count)
			{
				DisplayInvalidIndexError(index);
				return null;
			}

			return TextList[index];
		}

		/// <summary>
		/// Removes a Text instance from the internal list. 
		/// This method can only be accessed when in editor mode.
		/// </summary>
		/// <param name="index">A valid index value (zero based value).</param>
		public void RemoveTextInstance(int index)
		{
			if (Application.isEditor == false || Application.isPlaying == true)
			{
				DisplayMethodAccessViolation(this.ToString());
				return;
			}

			if (index < 0 || index > TextList.Count)
			{
				DisplayInvalidIndexError(index);
				return;
			}

			TextList.RemoveAt(index);
		}

		/// <summary>
		/// Adds a new Text instance to the internal list. The new Text instance is
		/// added last in the list. This method can only be accessed when in editor mode.
		/// </summary>
		/// <param name="newTextInstance">The new Text instance to add to the internal list.</param>
		public void AddTextInstance(TextInstance newTextInstance)
		{
			if (Application.isEditor == false || Application.isPlaying == true)
			{
				DisplayMethodAccessViolation(this.ToString());
				return;
			}

			TextList.Add(newTextInstance);
		}

		/// <summary>
		/// Internal method to create a error message if a method tries to access a element in the internal
		/// Text instance list which doesn't exist.
		/// </summary>
		/// <param name="index">A valid index value (zero based value).</param>
		private void DisplayInvalidIndexError(int index)
		{
			StringBuilder errorText = new StringBuilder();
			errorText.Append("index value (");
			errorText.Append(index);
			errorText.Append(") is not valid. Value must be between 0 and ");
			errorText.Append(TextList.Count);
			errorText.Append(".");
			Debug.LogError(errorText.ToString());
		}

		/// <summary>
		/// Internal method to create a error message if a method tries to access a method when the application
		/// is running inside or outside the editor. The method is only accessable in editor mode inside the editor.
		/// </summary>
		/// <param name="methodNameString">Method which activated the method.</param>
		private void DisplayMethodAccessViolation(string methodNameString)
		{
			StringBuilder errorText = new StringBuilder();
			errorText.Append(methodNameString);
			errorText.Append(" - This method can only be accessed in editor mode.");
			Debug.LogError(errorText.ToString());
		}
	}
}
