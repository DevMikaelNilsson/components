using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// The script checks for matching node objects.
/// The script uses recursive functions to go through all objects (horizontal/right and vertical/downwards) in a given list of
/// available node objects. If the total matched objects is larger than the minimum required matches, the value will be returned.\n
/// Otherwise the return value will always be 0.
/// </summary>
[AddComponentMenu("Utility scripts/MatchObjects")]
public class MatchObjects : MonoBehaviour 
{
	/// <summary>
	/// The available types to perform the match with.
	/// </summary>
	public enum MatchByType
	{
		/// <summary>
		/// Uses the material(s) of each object to match. Only one material must be a match in order for the objects to be counted as a match.
		/// If a object has multiple materials assigned to it, only one of these materials needs to be a match against the other objects' material(s).
		/// </summary>
		Material = 0
	}
	
	/// <summary>
	/// The type given here will be the matching base for all objects.
	/// </summary>
	public MatchByType MatchObjectsByType					= MatchByType.Material;
	
	/// <summary>
	/// The minimum number of matches required to be counted as a match.
	/// </summary>
	public int MinimumNumberOfMatches						= 3;
	
	/// <summary>
	/// The number of horizontal objects in a horizontal row (left to right).
	/// </summary>
	public int NumberOfHorizontalObjects					= 8;
	
	/// <summary>
	/// The number of vertical objects in a vertical row (up to down).
	/// </summary>
	public int NumberOfVerticalObjects						= 8;
	
	/// <summary>
	/// Internal object list which contains all horizontally matched node objects.
	/// </summary>
	private List<GameObject> m_horizontalObjectList			= new List<GameObject>();
	
	/// <summary>
	/// Internal object list which contains all vertically matched node objects.
	/// </summary>
	private List<GameObject> m_verticalObjectList			= new List<GameObject>();
	
	/// <summary>
	/// Internal object list of which matched objects that can/will be removed.
	/// </summary>
	private List<GameObject> m_removeObjectList				= new List<GameObject>();
		
	/// <summary>
	/// Checks for matching objects using a object list.
	/// </summary>
	/// <param name='objectList'>
	/// List of all objects to perform a match against.
	/// </param>
	/// <param name='checkHorizontal'>
	/// Set to true if the method should perform checks in the horizontal row.
	/// Set to false if the method should ignore the horizontal objects.
	/// </param>
	/// <param name='checkVertical'>
	/// Set to true if the method should perform checks in the vertical row.
	/// Set to false if the method should ignore the vertical objects.
	/// </param>
	private void CheckForMatchingNodesFromList(List<GameObject> objectList, bool checkHorizontal, bool checkVertical)
	{		
		int currentHorizontal 	= 0;
		int currentVertical		= 0;
		ClearInternalLists();
		
		if(checkHorizontal == false && checkVertical == false)
		{
			Debug.LogError(this.ToString() + " - Both horizontal and vertical checks are disabled. Can not perform checks.");
			return;	
		}
		
		for(int i = 0; i < objectList.Count; ++i)
		{	
			int foundHorizontalMatches 	= 0;
			int foundVerticalMatches	= 0;
			if(objectList[i] != null)
			{
				if(checkHorizontal == true)
					foundHorizontalMatches = CheckHorizontalNodes(1, i, objectList[i], objectList, currentHorizontal);
				if(checkVertical == true)
					foundVerticalMatches = CheckVerticalNodes(1, i, objectList[i], objectList, currentVertical);
			}
			
			if(foundHorizontalMatches >= MinimumNumberOfMatches || foundVerticalMatches >= MinimumNumberOfMatches)
			{
				if(checkHorizontal == true)
					AddMatchingNodeToList(m_horizontalObjectList, objectList[i]);
				else
					AddMatchingNodeToList(m_verticalObjectList, objectList[i]);
			}	
				
			currentHorizontal++;	
			if(currentHorizontal >= (NumberOfHorizontalObjects))
			{
				currentVertical++;
				currentHorizontal = 0;
			}
		}
	}
	
	/// <summary>
	/// Retrieves the number of matched objects. In order to work properly, a match method must first be called,
	/// otherwise the method always return 0. 
	/// </summary>
	/// <returns>
	/// The number of registered matches. Returns 0 if no match method has been called previous to this method.
	/// </returns>
	/// <param name='addMatchingObjectsToRemovalList'>
	/// Set to true if all matching objects should be added to the removal list.
	/// Set to false if no matching objects should be added to the removal list.
	/// </param>
	private int GetMatchedObjectCount(bool addMatchingObjectsToRemovalList)
	{
		m_removeObjectList.Clear();
		
		if(addMatchingObjectsToRemovalList == true)
		{
			for(int i = 0; i < m_horizontalObjectList.Count; ++i)
				AddObjectToList(m_horizontalObjectList[i]);
				
			for(int i = 0; i < m_verticalObjectList.Count; ++i)
				AddObjectToList(m_verticalObjectList[i]);
		
			ClearInternalLists();
			return m_removeObjectList.Count;
		}
		
		int returnValue = (m_horizontalObjectList.Count + m_verticalObjectList.Count);		
		ClearInternalLists();
		return returnValue;
	}
	
	/// <summary>
	/// Checks for horizontal object matches.
	/// </summary>
	/// <returns>
	/// The number of found matches in the horizontal row/s.
	/// </returns>
	/// <param name='objectList'>
	/// Object list with all objects to perform matches against.
	/// </param>
	public int CheckForHorizontalObjectMatches(List<GameObject> objectList)
	{
		CheckForMatchingNodesFromList(objectList, true, false);
		return GetMatchedObjectCount(true);
	}
	
	/// <summary>
	/// Checks for vertical object matches.
	/// </summary>
	/// <returns>
	/// The number of found matches in the vertical row/s.
	/// </returns>
	/// <param name='objectList'>
	/// Object list with all objects to perform matches against.
	/// </param>
	public int CheckForVerticalObjectMatches(List<GameObject> objectList)
	{
		CheckForMatchingNodesFromList(objectList, false, true);
		return GetMatchedObjectCount(true);
	}
	
	/// <summary>
	/// Checks for matches both in the horizontal and vertical row/s.
	/// </summary>
	/// <returns>
	/// The number of found matches in all row/s.
	/// </returns>
	/// <param name='objectList'>
	/// Object list with all objects to perform matches against.
	/// </param>	
	public int CheckForObjectMatches(List<GameObject> objectList)
	{
		CheckForMatchingNodesFromList(objectList, true, true);
		return GetMatchedObjectCount(true);
	}
	
	/// <summary>
	/// Checks for horizontal object match count.
	/// </summary>
	/// <returns>
	/// The number of found matches in the horizontal row/s.
	/// </returns>
	/// <param name='objectList'>
	/// Object list with all objects to perform matches against.
	/// </param>	
	public int GetHorizontalMatchCount(List<GameObject> objectList)
	{
		CheckForMatchingNodesFromList(objectList, true, false);
		return GetMatchedObjectCount(false);
	}
	
	/// <summary>
	/// Checks for vertical object match count.
	/// </summary>
	/// <returns>
	/// The number of found matches in the vertical row/s.
	/// </returns>
	/// <param name='objectList'>
	/// Object list with all objects to perform matches against.
	/// </param>
	public int GetVerticalMatchCount(List<GameObject> objectList)
	{
		CheckForMatchingNodesFromList(objectList, false, true);
		return GetMatchedObjectCount(false);
	}
	
	/// <summary>
	/// Checks for match count in the horizontal and vertical row/s.
	/// </summary>
	/// <returns>
	/// The number of found matches in all row/s.
	/// </returns>
	/// <param name='objectList'>
	/// Object list with all objects to perform matches against.
	/// </param>
	public int GetObjectMatchCount(List<GameObject> objectList)
	{
		CheckForMatchingNodesFromList(objectList, true, true);
		return GetMatchedObjectCount(false);
	}
	
	/// <summary>
	/// Clears the internal lists. This can not be undone!
	/// </summary>
	private void ClearInternalLists()
	{
		m_horizontalObjectList.Clear();
		m_verticalObjectList.Clear();
	}	
	
	/// <summary>
	/// Add a object to the removal list. The method also makes sure that there will not be any duplicates.
	/// </summary>
	/// <param name='addObjectScriptToList'>
	/// The object to add to the list.
	/// </param>
	private void AddObjectToList(GameObject objectToAdd)
	{
		for(int i = 0; i < m_removeObjectList.Count; ++i)
			if(m_removeObjectList[i] == objectToAdd)
				return;
				
		m_removeObjectList.Add(objectToAdd);
	}
	
	/// <summary>
	/// Removes all matched objects from the game.
	/// The removal of the objects needs to be called after all object has been checked for else there might be a
	/// risk that some matched combination will be missed since one or several objects have been removed.
	/// </summary>
	/// <returns>
	/// The number of matched/removed objects.
	/// </returns>
	public int RemoveMatchedObjects()
	{
		int objectCount = m_removeObjectList.Count;
		for(int i = 0; i < objectCount; ++i)
		{
			if(m_removeObjectList[i] == null || m_removeObjectList[i].activeSelf == false)
				continue;
				
			Destroy(m_removeObjectList[i]);
		}
		
		m_removeObjectList.Clear();
		return objectCount;
	}
				
	/// <summary>
	/// Recursive function which scans a nodes neighbours horizontally to the right of the node.
	/// First the node get the neighbour horizontal value and checks if the both nodes have the same setup.
	/// If the two nodes are a match, the same function will be called by the neighbour node, which checks its neighbours etc..
	/// This goes on until either two nodes does not match, or that the recursive function has reached the end of the horizontal grid.
	/// When the a end is found, the total matched value is return backwards.
	/// </summary>
	/// <returns>
	/// The number of found horizontal matches.
	/// </returns>
	/// <param name='startMatchValue'>
	/// The starting match value. For every match found this value will increase by one.
	/// </param>
	/// <param name='iteratorIndex'>
	/// The current iterator index value for the main object which all other objects will be compared to.
	/// </param>
	/// <param name='currentObjectNode'>
	/// Current object node which all other objects will be compared to.
	/// </param>
	/// <param name='objectNodeList'>
	/// List of all objects which the matches will be made against.
	/// </param>
	/// <param name='horizontal'>
	/// The current horizontal value for the current object which all the comparisons will be made against.
	/// </param>
	private int CheckHorizontalNodes(int startMatchValue, int iteratorIndex, GameObject currentObjectNode, List<GameObject> objectNodeList, int horizontal)
	{
		int newHorizontalValue 		= (horizontal + 1);
		int neighbourIteratorIndex 	= (iteratorIndex + 1);
		if(newHorizontalValue >= NumberOfHorizontalObjects)
			return startMatchValue;

		GameObject matchingNeighbour = CheckNode(currentObjectNode, objectNodeList[neighbourIteratorIndex]);
				
		if(matchingNeighbour != null)
		{
			if(AddMatchingNodeToList(m_horizontalObjectList, matchingNeighbour) == true)
				return CheckHorizontalNodes((startMatchValue+1), neighbourIteratorIndex, matchingNeighbour, objectNodeList, newHorizontalValue);
		}
		
		return startMatchValue;
	}
	
	/// <summary>
	/// Recursive function which scans a nodes neighbours vertically downwards from the node.
	/// First the node get the neighbour verticaL value and checks if the both nodes have the same setup.
	/// If the two nodes are a match, the same function will be called by the neighbour node, which checks its neighbours etc..
	/// This goes on until either two nodes does not match, or that the recursive function has reached the end of the vertical grid.
	/// When the a end is found, the total matched value is return backwards.
	/// </summary>
	/// <returns>
	/// The number of found vertical matches.
	/// </returns>
	/// <param name='startMatchValue'>
	/// The starting match value. For every match found this value will increase by one.
	/// </param>
	/// <param name='iteratorIndex'>
	/// The current iterator index value for the main object which all other objects will be compared to.
	/// </param>
	/// <param name='currentObjectNode'>
	/// Current object node which all other objects will be compared to.
	/// </param>
	/// <param name='objectNodeList'>
	/// List of all objects which the matches will be made against.
	/// </param>
	/// <param name='vertical'>
	/// The current vertical value for the current object which all the comparisons will be made against.
	/// </param>
	private int CheckVerticalNodes(int startMatchValue, int iteratorIndex, GameObject currentObjectNode, List<GameObject> objectNodeList, int vertical)
	{
		int newVerticalValue 		= (vertical + 1);
		int neighbourIteratorIndex 	= (iteratorIndex + NumberOfHorizontalObjects);
		if(newVerticalValue >= NumberOfVerticalObjects)
			return startMatchValue;

		GameObject matchingNeighbour = CheckNode(currentObjectNode, objectNodeList[neighbourIteratorIndex]);
				
		if(matchingNeighbour != null)
		{
			if(AddMatchingNodeToList(m_verticalObjectList, matchingNeighbour) == true)
				return CheckVerticalNodes((startMatchValue+1), neighbourIteratorIndex, matchingNeighbour, objectNodeList, newVerticalValue);
		}
		
		return startMatchValue;
	}	
		
	/// <summary>
	/// If a matching node is found. It should be added to a list for later usage.
	/// The function first checks so that the node isn't already added to the list.
	/// </summary>
	/// <returns>
	/// True if the node is added to the list.\n
	/// False if the node is already in the list.
	/// </returns>
	/// <param name='nodeList'>
	/// The list to add the matched object to.
	/// </param>
	/// <param name='matchingNode'>
	/// The matched node which will be added to the list.
	/// </param>
	private bool AddMatchingNodeToList(List<GameObject> objectNodeList, GameObject matchingNode)
	{
		int listCount = objectNodeList.Count;
		for(int i = 0; i < listCount; ++i)
		{
			if(objectNodeList[i] == matchingNode)
				return false;
		}
		
		objectNodeList.Add(matchingNode);
		return true;
	}
	
	/// <summary>
	/// Checks two nodes if they have the same type.
	/// </summary>
	/// <returns>
	/// The matching node if there is a 100% match.\n
	/// Returns null if there is no match between the nodes.
	/// </returns>
	/// <param name='currentNode'>
	/// The main node the matching node will be compared against.
	/// </param>
	/// <param name='matchingNode'>
	/// The matching node which will be compared against the current node.
	/// </param>
	private GameObject CheckNode(GameObject currentNode, GameObject matchingNode)
	{		
		if(currentNode.renderer == null || matchingNode.renderer == null)
		{
			Debug.LogError(this.ToString() + " - One or both objects (" + currentNode.name + ", " + matchingNode.name + ") are missing a render component. Cannot proceed with object matching.");
			return null;
		}
			
		switch(MatchObjectsByType)
		{
			case MatchByType.Material:
				Material []currentMaterials 	= currentNode.renderer.materials;
				Material []matchingMaterials 	= matchingNode.renderer.materials;
								
				for(int i = 0; i < currentMaterials.Length; ++i)
				{
					for(int j = 0; j < matchingMaterials.Length; ++j)
					{
						if(currentMaterials[i].name.CompareTo(matchingMaterials[j].name) == 0)
							return matchingNode;
					}
				}				
				break;
			default:
				return null;
		}
		
		return null;
	}
}
