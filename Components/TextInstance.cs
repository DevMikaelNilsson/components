using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// This is a single text instance which contains the Text.
/// </summary>
namespace Components
{
	[Serializable]
	public class TextInstance
	{
		/// <summary>
		/// Each language can have its own text string which both are stored here.
		/// </summary>
		[Serializable]
		public class TextBasedOnLanguage
		{
			/// <summary>
			/// The language which this instance is connected to. The string should normally be the current languages ISO code.
			/// </summary>
			public string Language = string.Empty;

			/// <summary>
			/// The text string which is connected to the current language.
			/// </summary>
			public string Text = string.Empty;
		}

		/// <summary>
		/// A tag string which is connected to this text instance. The tag is the same regardless of language and needs to be the proper Tag which
		/// is later collected from the  database.
		/// </summary>
		public string Tag = string.Empty;

		[SerializeField]
		private string m_internalDefaultString = string.Empty;

		[SerializeField]
		private bool m_isObjectLocked = false;
		[SerializeField]
		private List<TextBasedOnLanguage> m_internalLanguageTextList = new List<TextBasedOnLanguage>();
		[SerializeField]
		private const string m_languageIsoCodeString = "IsoLanguageCode";

		/// <summary>
		/// Class constructor. The inparameters can only be set once at construction and not later.
		/// </summary>
		/// <param name="defaultStringText">The default string which will be set by default on all languages.</param>
		/// <param name="isInstanceLocked">Set to true if the text instance can not change any of its texts or settings.</param>
		public TextInstance(string defaultStringText, bool isInstanceLocked)
		{
			m_internalDefaultString = defaultStringText;
			m_isObjectLocked = isInstanceLocked;
		}

		/// <summary>
		/// Returns true if the text instance is flagged as locked. Return false otherwise.
		/// </summary>
		public bool IsTextInstanceLocked { get { return m_isObjectLocked; } }

		/// <summary>
		/// Retrieves the proper text string based on the current language settings. 
		/// </summary>
		/// <returns>The string set for the current language. If the method is not succesfull, the method returns string.Empty.</returns>
		private string GetInternalText()
		{
			string currentLanguage = String.Empty;
			currentLanguage = PlayerPrefs.GetString(m_languageIsoCodeString, string.Empty);

			if (String.Equals(currentLanguage, String.Empty) == true)
			{
				Debug.LogError(this + " - Unable to locate a ISO string. Reverting to english (eng).");
				currentLanguage = "eng";
			}


			int objectCount = m_internalLanguageTextList.Count;
			for (int i = 0; i < objectCount; ++i)
			{
				if (string.Equals(m_internalLanguageTextList[i].Language, currentLanguage) == true)
					return m_internalLanguageTextList[i].Text;
			}

			Debug.LogWarning(this + " - Current ISO language string (" + currentLanguage + ") is not registered. Can not return internal text.");
			return string.Empty;
		}

		/// <summary>
		/// Overwrites the existing debug text, or if any text does not exist, adds the text to the text instance.
		/// This method only works when in editor mode for debugging purposes. There needs to be a valid registry entry for a language
		/// in order for this mtehod to succeed. The language is set through the Controller editor.
		/// </summary>
		/// <param name="newText">The new text to add to the text instance debug text string.</param>
		public void SetDebugText(string newText)
		{
			if (Application.isEditor == false)
			{
				Debug.LogError("This action is only allowed in editor mode.");
				return;
			}

			string currentLanguage = PlayerPrefs.GetString(m_languageIsoCodeString, string.Empty);

			if (currentLanguage == string.Empty)
			{
				Debug.LogError("Registry entry '" + m_languageIsoCodeString + "' returned a invalid string (string.Empty).");
				return;
			}

			int objectCount = m_internalLanguageTextList.Count;
			for (int i = 0; i < objectCount; ++i)
			{
				if (string.Equals(m_internalLanguageTextList[i].Language, currentLanguage) == true)
				{
					m_internalLanguageTextList[i].Text = newText;
					return;
				}
			}

			// If the text instance is not found, then I create a new one and add the text.
			// If the text instance is flagged as locked, then I simply add the default text string instead of the new text.
			TextBasedOnLanguage newLanguageInstance = new TextBasedOnLanguage();
			newLanguageInstance.Language = currentLanguage;
			if (m_isObjectLocked == false)
				newLanguageInstance.Text = newText;
			else
				newLanguageInstance.Text = m_internalDefaultString;

			m_internalLanguageTextList.Add(newLanguageInstance);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns>A valid text string.</returns>
		public string GetText()
		{
			return GetInternalText();
		}
	}
}
