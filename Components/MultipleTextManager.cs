using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Components
{
	/// <summary>
	/// MultipleTextManager is an extension to the TextManager class.
	/// This script handles multiple TextManager objects (prefabs) at once, so that a user can use unlimited number of premade prefabs in a scene without
	/// the need to rewrite a new one every time.
	/// 
	/// Ex.
	/// The user has a global PauseMenuText.prefab (a TextManager object with number of texts) which should be accessed through all his gameplay scenes (4 different scenes in total).
	/// But each individual gameplay scene has its own prefab text object (a TextManager object with number of texts). So the user has these two options:
	/// 1. Add all PauseMenuText texts into each specific gameplay prefab object.
	/// 2. Add the PauseMenuText.prefab and the specific gameplay prefab object to the scene, and access them individually. (ex. PauseMenuText.instance.GetText and GamePlayScene1.instanceGetText).
	///
	/// But the user can also add all the prefabs into the prefab list of this script, and access all texts through this single object (MultipleTextManager.instance.GetText),
	/// making it easier to keep all texts under control and without needed to have multiple object calls in different scripts. 
	/// </summary>
	[AddComponentMenu("Components/Text/MultipleTextManager")]
	public class MultipleTextManager : MonoBehaviour
	{
		/// <summary>
		/// List of all prefab objects the class can access.
		/// </summary>
		public List<GameObject> TextPrefabList = new List<GameObject>();

		/// <summary>
		/// Internal list which stores all the Text instances, which are collected from all the accessed prefab objects.
		/// </summary>
		private List<TextInstance> m_combinedTextInstanceList = new List<TextInstance>();

		/// The MultipleTextManager Instance.
		private static MultipleTextManager m_currentInstance = null;

		/// <summary>
		/// Get the class instance, or if it doesn't exist, create a new one.
		/// </summary>
		public static MultipleTextManager Instance
		{
			get
			{
				if (m_currentInstance == null)
				{
					m_currentInstance = (MultipleTextManager)FindObjectOfType(typeof(MultipleTextManager));
				}

				// If it is still null, create a new instance
				if (m_currentInstance == null)
				{
					GameObject obj = new GameObject("MultipleTextManager");
					m_currentInstance = (MultipleTextManager)obj.AddComponent(typeof(MultipleTextManager));
				}

				return m_currentInstance;
			}
		}

		/// <summary>
		/// Internal Unity method. This method is only called once per lifetime/scene.
		/// Everytime the object is awaken, I recreate the list to keep it up to date.
		/// </summary>
		void Awake()
		{
			m_combinedTextInstanceList.Clear();
			for (int prefabIndex = 0; prefabIndex < TextPrefabList.Count; ++prefabIndex)
			{
				TextManager currentInstance = TextPrefabList[prefabIndex].GetComponent<TextManager>();
				if (currentInstance != null)
				{
					int objectCount = currentInstance.ObjectCount;

					for (int textInstanceIndex = 0; textInstanceIndex < objectCount; ++textInstanceIndex)
						m_combinedTextInstanceList.Add(currentInstance.GetTextInstance(textInstanceIndex));
				}
			}
		}

		/// <summary>
		/// Retrieves a text string based on the given tag string.
		/// If there are multiple Text objects with the same Tag string, the first
		/// text object which is found in the list is returned.
		/// </summary>
		/// <param name="tag">Tag which the text is retrieved from. This value can not be string.Empty.</param>
		/// <returns>A text string if found. Returns null if the Tag string is not found, or if the string is empty.
		/// The returning string is also based on which current language is set in the  Controller.</returns>
		public string GetText(string tag)
		{
			if (tag == string.Empty)
			{
				Debug.LogError(this.ToString() + " - Invalid Tag value (" + tag + ").");
				return string.Empty;
			}

			int objectCount = m_combinedTextInstanceList.Count;
			for (int i = 0; i < objectCount; ++i)
			{
				if (string.Equals(m_combinedTextInstanceList[i].Tag, tag) == true)
				{
					return m_combinedTextInstanceList[i].GetText();
				}
			}

			return string.Empty;
		}

	}
}
