using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Components;

/// <summary>
/// Editor script for the PhoenixTextManager class.
/// </summary>
[CustomEditor(typeof(TextManager))]
public class TextManagerEditor : Editor
{
	private TextManager m_currentTextInstance = null;
	private string m_currentLanguage = string.Empty;
	private const string m_languageIsoCodeString = TextLanguageManager.LanguageIsoCodeString;

	/// <summary>
	/// When the custom inspector is activated, normally when the user has clicked on the object which the inspector is attached to.
	/// </summary>
	public void OnEnable()
	{
		m_currentTextInstance = null;
		GameObject currentTargetObject = Selection.activeGameObject;
		if (currentTargetObject != null)
		{
			m_currentTextInstance = currentTargetObject.GetComponent<TextManager>();
			m_currentTextInstance.ReloadTextList();
		}
		else
		{
			Debug.LogError("Selected active GameObject could not be found.");
		}
	}

	/// <summary>
	/// Editor update method.
	/// </summary>
	override public void OnInspectorGUI()
	{
		// Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
		serializedObject.Update();

		EditorGUILayout.BeginVertical("Box");

		if (m_currentTextInstance != null)
		{

			DisplayCurrentLanguage();
			DisplayAddButton();
			DisplayTextList();
		}
		else
			EditorGUILayout.LabelField("PhoenixTextManager component could not be found.");

		EditorGUILayout.EndVertical();
		// Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
		serializedObject.ApplyModifiedProperties();

	}

	/// <summary>
	/// Displays the current language set through the Phoenix controller.
	/// </summary>
	private void DisplayCurrentLanguage()
	{
		EditorGUILayout.BeginVertical("Box");
		string currentLanguage = PlayerPrefs.GetString(m_languageIsoCodeString, string.Empty);
		if (string.Equals(currentLanguage, string.Empty) == true)
			currentLanguage = "No language available.";

		StringBuilder labelString = new StringBuilder();
		labelString.Append("Current ISO Code Language: ");
		labelString.Append(currentLanguage);

		EditorGUILayout.LabelField(labelString.ToString(), EditorStyles.boldLabel);
		EditorGUILayout.EndVertical();
	}

	/// <summary>
	/// Displays a add button, which the user can add additional Text instances.
	/// </summary>
	private void DisplayAddButton()
	{
		EditorGUILayout.BeginHorizontal();
		if (GUILayout.Button("Add text instance") == true)
		{
			TextInstance newTextInstance = new TextInstance(string.Empty, false);
			m_currentTextInstance.AddTextInstance(newTextInstance);
		}

		EditorGUILayout.EndHorizontal();
		EditorGUILayout.Space();
	}
	SerializedObject cobject;

	/// <summary>
	/// Displays all elements in the text instance list.
	/// </summary>
	private void DisplayTextList()
	{
		string isoLanguageString = "eng";
		if (string.Equals(m_currentLanguage, isoLanguageString) == false)
		{
			m_currentTextInstance.ReloadTextList();
			m_currentLanguage = isoLanguageString;
		}

		int objectCount = m_currentTextInstance.ObjectCount;
		for (int i = 0; i < objectCount; ++i)
		{
			EditorGUILayout.BeginVertical("Box");

			TextInstance currentTextInstance = m_currentTextInstance.GetTextInstance(i);

			string DebugText = currentTextInstance.GetText();
			string currentTag = currentTextInstance.Tag;
			bool isTextLocked = currentTextInstance.IsTextInstanceLocked;

			if (isTextLocked == false)
			{
				currentTextInstance.Tag = EditorGUILayout.TextField("Tag: ", currentTag);
				DebugText = EditorGUILayout.TextField("Debug text: ", DebugText);
			}
			else
			{
				EditorGUILayout.LabelField("Tag: ", currentTag);
				EditorGUILayout.LabelField("Debug text: ", DebugText);
			}

			currentTextInstance.SetDebugText(DebugText);

			if (isTextLocked == false)
			{
				if (GUILayout.Button("Remove") == true)
				{
					m_currentTextInstance.RemoveTextInstance(i);
					return;
				}
			}

			EditorGUILayout.EndVertical();
		}
	}
}
