using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Components;
using System.Text;

/// <summary>
/// Editor script for the PhoenixMultipleTextManager class.
/// </summary>
[CustomEditor(typeof(MultipleTextManager))]
public class MultipleTextManagerEditor : Editor
{
	private SerializedProperty m_prefabObjectList;
	private const string m_languageIsoCodeString = TextLanguageManager.LanguageIsoCodeString;

	/// <summary>
	/// When the custom inspector is activated, normally when the user has clicked on the object which the inspector is attached to.
	/// </summary>
	public void OnEnable()
	{
		m_prefabObjectList = serializedObject.FindProperty("TextPrefabList");
	}

	/// <summary>
	/// Editor update method.
	/// </summary>
	override public void OnInspectorGUI()
	{
		// Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
		serializedObject.Update();

		EditorGUILayout.BeginVertical("Box");

		DisplayCurrentLanguage();
		DisplayAddButton();
		DisplayPrefabObjectList();

		EditorGUILayout.EndVertical();
		// Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
		serializedObject.ApplyModifiedProperties();
	}

	/// <summary>
	/// Displays the current language set through the Phoenix controller.
	/// </summary>
	private void DisplayCurrentLanguage()
	{
		EditorGUILayout.BeginVertical("Box");
		string currentLanguage = PlayerPrefs.GetString(m_languageIsoCodeString, string.Empty);
		if (string.Equals(currentLanguage, string.Empty) == true)
			currentLanguage = "No language available.";

		StringBuilder labelString = new StringBuilder();
		labelString.Append("Current ISO Code Language: ");
		labelString.Append(currentLanguage);

		EditorGUILayout.LabelField(labelString.ToString(), EditorStyles.boldLabel);
		EditorGUILayout.EndVertical();
	}

	/// <summary>
	/// Displays a Add button in the custom inspector which allows the user to add a additional 
	/// element to the prefab object list.
	/// </summary>
	private void DisplayAddButton()
	{
		if (GUILayout.Button("Add prefab object") == true)
		{
			int currentIndex = m_prefabObjectList.arraySize;
			m_prefabObjectList.InsertArrayElementAtIndex(currentIndex);
			SerializedProperty currentInstance = m_prefabObjectList.GetArrayElementAtIndex(currentIndex);
			currentInstance.objectReferenceValue = null;
		}
	}

	/// <summary>
	/// Loops through all existing elements in the prefab object list and displays its data.
	/// A remove button is added to each element which, when pressed, removes the current element from the list.
	/// </summary>
	private void DisplayPrefabObjectList()
	{
		int objectCount = m_prefabObjectList.arraySize;
		for (int i = 0; i < objectCount; ++i)
		{
			EditorGUILayout.BeginVertical("Box");
			EditorGUILayout.LabelField("Prefab object:");
			EditorGUILayout.BeginHorizontal("Box");

			SerializedProperty currentInstance = m_prefabObjectList.GetArrayElementAtIndex(i);
			currentInstance.objectReferenceValue = EditorGUILayout.ObjectField("", currentInstance.objectReferenceValue, typeof(GameObject), false);
			if (GUILayout.Button("Remove") == true)
			{
				currentInstance.objectReferenceValue = null;
				m_prefabObjectList.DeleteArrayElementAtIndex(i);
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.EndVertical();
				break;
			}

			EditorGUILayout.EndHorizontal();
			EditorGUILayout.EndVertical();
		}


	}
}
