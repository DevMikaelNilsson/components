using UnityEngine;
using UnityEditor;
using System.Collections;
using Components;

[CustomEditor(typeof(TextLanguageManager))]
public class TextLanguageManagerEditor : Editor 
{
	private SerializedProperty m_availableTextList;
	private SerializedProperty m_currentLanguageIndex;

	private const string m_languageIsoCodeString = TextLanguageManager.LanguageIsoCodeString;

	/// <summary>
	/// When the custom inspector is activated, normally when the user has clicked on the object which the inspector is attached to.
	/// </summary>
	public void OnEnable()
	{
		m_availableTextList = serializedObject.FindProperty("LanguageList");
		m_currentLanguageIndex = serializedObject.FindProperty("CurrentLanguageIndex");
	}

	/// <summary>
	/// Editor update method.
	/// </summary>
	override public void OnInspectorGUI()
	{
		// Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
		serializedObject.Update();

		DisplayLanguages();

		DisplayAvailableTextList();

		// Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
		serializedObject.ApplyModifiedProperties();

	}

	private void DisplayLanguages()
	{
		string []languages = new string[m_availableTextList.arraySize];
		int objectCount = m_availableTextList.arraySize;
			for(int i = 0; i < objectCount; ++i)
				languages[i] = m_availableTextList.GetArrayElementAtIndex(i).stringValue;

		string currentLanguage = PlayerPrefs.GetString(m_languageIsoCodeString, string.Empty);

		if(string.Equals(currentLanguage, string.Empty) == false)
		{
			objectCount = languages.Length;
			for(int i = 0; i < objectCount; ++i)
			{
				if(string.Equals(languages[i], currentLanguage) == true)
				{
					m_currentLanguageIndex.intValue = i;
					break;
				}
			}
		}
		
		m_currentLanguageIndex.intValue = EditorGUILayout.Popup("Current language", m_currentLanguageIndex.intValue, languages);
		PlayerPrefs.SetString(m_languageIsoCodeString, languages[m_currentLanguageIndex.intValue]);
	}

	private void DisplayAvailableTextList()
	{
		if(GUILayout.Button("Add new language") == true)
		{
			m_availableTextList.InsertArrayElementAtIndex(m_availableTextList.arraySize);
			SerializedProperty currentTextElement = m_availableTextList.GetArrayElementAtIndex(m_availableTextList.arraySize - 1);
			currentTextElement.stringValue = string.Empty;
		}
		else
		{
			int objectCount = m_availableTextList.arraySize;
			for(int i = 0; i < objectCount; ++i)
			{
				SerializedProperty currentTextElement = m_availableTextList.GetArrayElementAtIndex(i);
				EditorGUILayout.BeginHorizontal("box");
				EditorGUILayout.PropertyField(currentTextElement);
				if(GUILayout.Button("Remove") == true)
				{
					m_availableTextList.DeleteArrayElementAtIndex(i);
					EditorGUILayout.EndHorizontal();
					break;
				}
				else
					EditorGUILayout.EndHorizontal();
			}
		}
	}
}
